<?php
class Usuario
{

    private $id;
    private $nome;
    private $email;
    private $senha;
    public $objDB;

    public function __construct()
    {
        $this->objDB = new mySQLi(
            'localhost',
            'root',
            '',
            'aulaPHP',
            3307
        );
        echo "conectado";
    }

    public function setId(int $id)
    {
        $this->$id = $id;
    }
    public function setNome(string $nome)
    {
        $this->$nome = $nome;
    }
    public function setEmail(string $email)
    {
        $this->$email = $email;
    }
    public function setSenha(string $senha)
    {
        $this->$senha = $senha;
    }
    public function getId(int $id) : int
    {
        return $this->$id;
    }
    public function getNome(string $nome) : string
    {
        return $this->$nome;
    }
    public function getEmail(string $email) : string
    {
        return $this->$email;
    }
    public function getSenha(string $senha) : string
    {
        return $this->$senha;
    }

    public function deleteUser()
    {
        $objStmt = $this->objDb->prepare("DELETE FROM tb_usuario WHERE id=?");

        $objStmt->bind_param("i", $this->id);

        $objStmt->execute();

    }

    public function listarUsuario()
    {
        $objListar = $this->objDb->query("SELECT 
                                            id, nome, email, senha 
                                            FROM 
                                            tb_usuario 
                                          ");
        return $objListar;
    }


    public function saveUser()
    {
        $objStmt = $this->objDb->prepare('REPLACE INTO tb_usuario (nome, email, senha) VALUES (?, ?, ?)');

        $objStmt->bind_param('sss', $this->nome, $this->email, $this->senha);

        if ($objStmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function __destruct()
    {
        unset($this->objDb);
        echo "<br>Fechando a conexão com o SGDB";
    }

}
