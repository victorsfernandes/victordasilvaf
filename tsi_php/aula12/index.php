<?php
require_once("crud.php");

echo "<pre>";

$objUsuario = new Usuario;

$objUsuario->setId(1);
$objUsuario->setNome('Guilherme');
$objUsuario->setEmail('guilherme@mail.com');
$objUsuario->setSenha('SuperSenha');
$objUsuario->saveUser();

$objUsuario->setId(2);
$objUsuario->setNome('Ferreira');
$objUsuario->setEmail('ferreira@mail.com');
$objUsuario->setSenha('SP');
$objUsuario->saveUser();

$objUsuario->deleteUser();

$objUsuario->setId(3);
$objUsuario->setNome('Batista');
$objUsuario->setEmail('batista@mail.com');
$objUsuario->setSenha('SuperPassword');
$objUsuario->saveUser();



$returnFunction = $objUsuario->listarUsuario();


echo "<table>
            <tr>
                <td>ID</td>
                <td>Nome</td>
                <td>Email</td>
                <td>Senha</td>
            </tr>";
while ($reg = $returnFunction->fetch_assoc()) {
    echo "	<tr>
                    <td>{$reg['id']}</td>
                    <td>{$reg['nome']}</td>
                    <td>{$reg['email']}</td>
                    <td>{$reg['senha']}</td>
                </tr>";
}
echo "</table>";